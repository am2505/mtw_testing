require ('custom-env').env(true);
const webdriver = require('selenium-webdriver'),
    chrome = require('selenium-webdriver/chrome'),
    path = require('path'),
    yandex_api_key = process.env.YANDEX_KEY,
    google_api_key = process.env.GOOGLE_KEY,
    azure_api_key = process.env.AZURE_KEY,
    By = webdriver.By;
    { describe, it} require('selenium-webdriver/testing')

require('chromedriver');
const mtw = path.resolve( __dirname, '../dist' );
var chromeOptions = new chrome.Options();
chromeOptions.addArguments("--no-sandbox");
chromeOptions.addArguments("--disable-gpu");
chromeOptions.addArguments("--disable-dev-shm-usage");
chromeOptions.addArguments( `--load-extension=/${ mtw }` );

let driver;
describe('Test Execution begin', function() {
    this.timeout(50000);

    describe('Create instance of Chrome', () => {
        it('instance of chrome is launching', (done) => {
            // This will create instance of chrome for testing extension  
            driver = new webdriver.Builder().setChromeOptions(chromeOptions).forBrowser('chrome').build();
            driver.then(() => {
                console.log('yandexkey is '+yandex_api_key);
                done();
            });
        });
    });
    describe('Construction of Extension', function() {
        this.timeout(30000);
        it('Opening Setting Extensions Page', function(done) {
            // This will open options.html page 
            driver.get('chrome-extension://fdokjffohfedmcnnoooamebpaobmblmf/views/options.html').then(() => {
                done();
            })
        });
        // checking Bootstrap Tour is working fine or not
        describe('Bootstrap Tour', function() {
            this.timeout(15000);
            it('Welcome screen', function(done) {
                driver.findElement(By.xpath('//*[@id="step-0"]/div[3]/div/button[2]')).click().then((e) => {
                    done();
                });
            });
            describe('Translation Tab', function(){
                this.timeout(10000);
                it('Translation Setting Tab', function(done) {
                    setTimeout(() => {
                        driver.findElement(By.xpath('//*[@id="step-1"]/div[3]/div/button[2]')).click().then((e) => {
                            done();
                        });
                    }, 1000);
                });
                it('Setting up Translator Keys', function(done) {
                    setTimeout(() => {
                        driver.findElement(By.xpath('//*[@id="step-2"]/div[3]/div/button[2]')).click().then((e) => {
                            done();
                        });
                    }, 1000);
                });
                it('Creating a new language pattern', function(done) {
                    setTimeout(() => {
                        driver.findElement(By.xpath('//*[@id="step-3"]/div[3]/div/button[2]')).click().then((e) => {
                            done();
                        });
                    }, 1000);
                });
                it('Managing language patterns', function(done) {
                    setTimeout(() => {
                        driver.findElement(By.xpath('//*[@id="step-4"]/div[3]/div/button[2]')).click().then((e) => {
                            done();
                        });
                    }, 1000);
                });
            });
            describe('Blacklist Tab', function(){
                this.timeout(10000);
                it('Blacklist Setting Tab', function(done) {
                    setTimeout(() => {
                        driver.findElement(By.xpath('//*[@id="step-5"]/div[3]/div/button[2]')).click().then((e) => {
                            done();
                        });
                    }, 1000);
                });
                it('Blacklisting Websites', function(done) {
                    setTimeout(() => {
                        driver.findElement(By.xpath('//*[@id="step-6"]/div[3]/div/button[2]')).click().then((e) => {
                            done();
                        });
                    }, 1000);
                });
                it('Blacklisting Words', function(done) {
                    setTimeout(() => {
                        driver.findElement(By.xpath('//*[@id="step-7"]/div[3]/div/button[2]')).click().then((e) => {
                            done();
                        });
                    }, 1000);
                });
            }); 
            describe('Learning Tab', function(){
                this.timeout(10000);
                it('Learning Setting Tab', function(done) {
                    setTimeout(() => {
                        driver.findElement(By.xpath('//*[@id="step-8"]/div[3]/div/button[2]')).click().then((e) => {
                            done();
                        });
                    }, 1000);
                });
                it('Saved Translations', function(done) {
                    setTimeout(() => {
                        driver.findElement(By.xpath('//*[@id="step-9"]/div[3]/div/button[2]')).click().then((e) => {
                            done();
                        });
                    }, 1000);
                });
                it('Learnt Words', function(done) {
                    setTimeout(() => {
                        driver.findElement(By.xpath('//*[@id="step-10"]/div[3]/button')).click().then((e) => {
                            done();
                        });
                    }, 1000);
                });
            });
            // similarly for advance tab and other
            // now we will end the tour             
        });
    });
    
    describe('Translation Pattern Testing', function() {
        this.timeout(15000);
        it('Going to Translation Pattern Page', function(done) {
            driver.get('chrome-extension://fdokjffohfedmcnnoooamebpaobmblmf/views/options.html').then(() => {
                done();
            })
        });
        describe('Creating Pattern with Yandex Random key', function(){
            this.timeout(10000);
            it('Adding Dummy API key for Yandex', function(done) {
                let dummyYandexKey = 'dummyyandexkey';
                driver.findElement(By.xpath('//*[@id="translator-keys"]/div[2]/div[1]/input')).then(field => {
                    field.sendKeys(dummyYandexKey);
                    done();
                });
            });

            it('Adding Dummy translation patterns', function(done) {
                driver.findElement(By.xpath('//*[@id="translator"]')).sendKeys('Yandex').then(() => {
                    driver.findElement(By.xpath('//*[@id="percentage"]')).sendKeys('30').then(() => {
                        driver.findElement(By.xpath('//*[@id="srcLang"]')).sendKeys('English').then(() => {
                            driver.findElement(By.xpath('//*[@id="targetLang"]')).sendKeys('Hindi').then(() => {
                                driver.findElement(By.xpath('//*[@id="createPatterns"]')).submit();
                                done();
                            });
                        });
                    });
                });
            });
            
            //  it('Deleting dummy translation pattern', function(done){
            //         driver.findElement(By.xpath('//*[@class="list-group-item ng-scope active-list"][1]/span[6]/input')).click().then((e) => {
            //        done();
            //     });
            //  });
        });
    });
});